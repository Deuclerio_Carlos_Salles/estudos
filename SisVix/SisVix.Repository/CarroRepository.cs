﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SisVix.Dominio.Contratos;
using SisVix.Dominio.Entidades;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SisVix.Repository
{
    public class CarroRepository : BaseRepositorio<Carro>, ICarroRepository
    {
        private readonly string _connectionString;
        public CarroRepository(IConfiguration configuration) : base(configuration)
        {
            _connectionString = base.ConnectionString;
        }

        public IEnumerable<Carro> InnerJoinCategoriaCarro()
        {
            return new List<Carro>();
        }

        public int InsertCarro(Carro entity)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var carro_id = con.Execute("insert into carro (ativo, marca, modelo, ano, cor, chassi, dtcadastro, possuiarcondicionado, possuidirecaohidraulica, categoria_id) " +
                    "values (@ativo, @marca, @modelo, @ano, @cor, @chassi, @dtcadastro, @possuiarcondicionado, @possuidirecaohidraulica, @categoria_id)", entity);
                return carro_id;
            }
        }
    }
}

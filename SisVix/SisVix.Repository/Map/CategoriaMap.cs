﻿using Dapper.FluentMap.Dommel.Mapping;
using SisVix.Dominio.Entidades;


namespace SisVix.Repository.Map
{
    class CategoriaMap : DommelEntityMap<Categoria>
    {
        public CategoriaMap()
        {
            ToTable("Categoria");

            Map(m => m.Categoria_Id)
                .ToColumn("Id")
                .IsKey()
                .IsIdentity();

            Map(m => m.NomeCategoria)
                .ToColumn("NomeCategoria");

            Map(m => m.PrecoBaseDiaria)
                .ToColumn("PrecoBaseDiaria");
        }

    }
}

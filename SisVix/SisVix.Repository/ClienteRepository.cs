﻿using Microsoft.Extensions.Configuration;
using SisVix.Dominio.Contratos;
using SisVix.Dominio.Entidades;


namespace SisVix.Repository
{
    public class ClienteRepository : BaseRepositorio<Cliente>, IClienteRepository
    {

        public ClienteRepository(IConfiguration configuration) : base(configuration)
        {
        }
    }
}

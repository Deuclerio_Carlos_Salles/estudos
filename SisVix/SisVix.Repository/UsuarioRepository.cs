﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SisVix.Dominio.Contratos;
using SisVix.Dominio.Entidades;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace SisVix.Repository
{
    public class UsuarioRepository : BaseRepositorio<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(IConfiguration configuration) : base(configuration)
        {
           
        }

        public Usuario Get(string email, string senha)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = String.Format("select * from usuario where email='{0}' and senha='{1}'", email, senha);
                var usuario = con.Query<Usuario>(query);
                return usuario.FirstOrDefault();
            }
        }

        public Usuario Get(string email)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = String.Format("select * from usuario where email='{0}'", email);
                var usuario = con.Query<Usuario>(query);
                return usuario.FirstOrDefault();
            }
        }


    }
}

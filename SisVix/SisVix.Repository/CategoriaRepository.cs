﻿using Microsoft.Extensions.Configuration;
using SisVix.Dominio.Contratos;
using SisVix.Dominio.Entidades;

namespace SisVix.Repository
{
    public class CategoriaRepository : BaseRepositorio<Categoria>, ICategoriaRepository
    {


        public CategoriaRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public Categoria GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Categoria GetId(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
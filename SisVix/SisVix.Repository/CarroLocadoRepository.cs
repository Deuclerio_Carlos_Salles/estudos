﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SisVix.Dominio.Contratos;
using SisVix.Dominio.Entidades;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SisVix.Repository
{
    public class CarroLocadoRepository : BaseRepositorio<CarroLocado>, ICarroLocadoRepository
    {
        private readonly string _connectionString;
        public CarroLocadoRepository(IConfiguration configuration) : base(configuration)
        {
            _connectionString = base.ConnectionString;
        }

        public int InsertCarroLocado(CarroLocado entity)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var carroLocado_id = con.Execute("insert into carrolocado (Carro_Id, DtPrimeiroDia, DtUltimoDia, DtReserva, QtdDiasLocado, Usuario_Id) " +
                    "values (@Carro_Id, @DtPrimeiroDia, @DtUltimoDia, @DtReserva, @QtdDiasLocado, @Usuario_Id); " +
                    "SELECT CAST(SCOPE_IDENTITY() as INT);", entity);

                return carroLocado_id;
            }
        }
    }
}

﻿using SisVix.Dominio.Entidades;

namespace SisVix.Dominio.Contratos
{
    public interface IUsuarioRepository : IBaseRepositorio<Usuario>
    {
        Usuario Get(string email, string senha);
        Usuario Get(string email);
    }
}

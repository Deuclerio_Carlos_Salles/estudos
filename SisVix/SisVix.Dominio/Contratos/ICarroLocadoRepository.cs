﻿using SisVix.Dominio.Entidades;

namespace SisVix.Dominio.Contratos
{
    public interface ICarroLocadoRepository : IBaseRepositorio<CarroLocado>
    {
        int InsertCarroLocado(CarroLocado entity);
    }
}

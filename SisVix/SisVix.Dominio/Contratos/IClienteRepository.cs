﻿using SisVix.Dominio.Entidades;

namespace SisVix.Dominio.Contratos
{
    public interface IClienteRepository : IBaseRepositorio<Cliente>
    {

    }
}

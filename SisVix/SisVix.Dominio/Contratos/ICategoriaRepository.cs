﻿using SisVix.Dominio.Entidades;

namespace SisVix.Dominio.Contratos
{
    public interface ICategoriaRepository : IBaseRepositorio<Categoria>
    {
        Categoria GetAll();
        Categoria GetId(int id);
    }
}

﻿using SisVix.Dominio.Entidades;
using System.Collections.Generic;

namespace SisVix.Dominio.Contratos
{    public interface ICarroRepository : IBaseRepositorio<Carro>
    {
        IEnumerable<Carro> InnerJoinCategoriaCarro();
        int InsertCarro(Carro entity);
    }
}

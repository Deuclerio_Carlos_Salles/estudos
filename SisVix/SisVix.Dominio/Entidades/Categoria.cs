﻿using Dapper.Contrib.Extensions;
using System;
using System.ComponentModel.DataAnnotations;

namespace SisVix.Dominio.Entidades
{
    [Table("Categoria")]
    public class Categoria
    {
        [Dapper.Contrib.Extensions.Key]
        public int Categoria_Id { get; set; }

        [Required(ErrorMessage = "Nome da Categoria deve ser preenchido", AllowEmptyStrings = false)]
        [StringLength(60, MinimumLength = 4)]
        public string NomeCategoria { get; set; }

        [Range(1, 99999, ErrorMessage = "Preço Base deve ser preenchido")]
        public decimal PrecoBaseDiaria { get; set; }
    }
}

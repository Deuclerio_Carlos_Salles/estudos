﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SisVix.Dominio.Contratos;
using SisVix.Dominio.Entidades;
using SisVix.Web.Validation;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SisVix.Web.Controllers
{
    [Route("api/[controller]")]
    public class CarroController : Controller
    {
        private readonly ICarroRepository _db;
        //private readonly ICategoriaRepositorio _dbCategoria;
        public CarroController(ICarroRepository carroRepositorio)
        {
            _db = carroRepositorio;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_db.GetList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] Carro carro)
        {
            try
            {
                var erros = Validacao.getValidationErros(carro);
                if (erros.Count() != 0)
                    return BadRequest(string.Join(". ", erros));

                carro.DtCadastro = DateTime.Now;
                carro.Carro_Id = _db.InsertCarro(carro);
                return Created("api/carro", carro);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
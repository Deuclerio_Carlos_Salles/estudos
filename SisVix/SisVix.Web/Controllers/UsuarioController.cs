﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SisVix.Dominio.Contratos;
using SisVix.Dominio.Entidades;
using SisVix.Web.Validation;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SisVix.Web.Controllers
{
    [Route("api/[controller]")]
    public class UsuarioController : Controller
    {
        private readonly IUsuarioRepository _db;
        public UsuarioController(IUsuarioRepository usuarioRepositorio)
        {
            _db = usuarioRepositorio;
        }

        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                var usuario = _db.Get(1);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] Usuario usuario)
        {
            try
            {
                var erros = Validacao.getValidationErros(usuario);
                if (erros.Count() != 0)
                    return BadRequest(string.Join(". ", erros));

                var usuarioCadastrado = _db.Get(usuario.Email);

                if (usuarioCadastrado != null)
                    return BadRequest("Usuário já cadastrado no sistema");

                _db.Insert(usuario);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("VerificarUsuario")]
        public ActionResult VerificarUsuario([FromBody] Usuario usuario)
        {
            try
            {
                var erros = Validacao.getValidationErros(usuario, true);
                if (erros.Count() != 0)
                    return BadRequest(string.Join(". ", erros));

                var usuarioRetorno = _db.Get(usuario.Email, usuario.Senha);

                if (usuarioRetorno != null)
                    return Ok(usuarioRetorno);

                return BadRequest("Usuário ou senha inválido.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

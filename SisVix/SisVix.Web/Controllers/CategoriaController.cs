﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SisVix.Dominio.Contratos;
using SisVix.Dominio.Entidades;
using SisVix.Web.Validation;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SisVix.Web.Controllers
{
    [Route("api/[controller]")]
    public class CategoriaController : Controller
    {
        private readonly ICategoriaRepository _db;
        public CategoriaController(ICategoriaRepository categoriaRepositorio)
        {
            _db = categoriaRepositorio;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_db.GetList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] Categoria categoria)
        {
            try
            {
                var erros = Validacao.getValidationErros(categoria);
                if (erros.Count() != 0)
                    return BadRequest(string.Join(". ", erros));

                _db.Insert(categoria);
                return Created("api/categoria", categoria);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
